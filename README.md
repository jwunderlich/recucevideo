# ReduceVideo

**Description**:
  
Linux shell script to **reduce** the **video** size in the entire directory with the **ffmpeg** tool, copying the new generated files with its own name plus "r" (for reduced).
This shell script is a good solution to reduce the size of the video in a quick way, e.g. if you want to send it with email or messenger.  
  
**instructions**:  
  
Copy the shell script to the ~/bin directory as root and make it executable.
  
**Limitations**:  
  
- The shell script renames files in the entire directory only.
  
**Known issues**:  
  
- Files without extension are not correctly processed
